trigger UpdateReportingRequirement on APEX_Account_Contact_Relationship__c (before insert, before update) {
    List<Reporting_Requirement__c> extRpt = new List<Reporting_Requirement__c>();
    for(APEX_Account_Contact_Relationship__c acr : Trigger.new){
        APEX_Account_Contact_Relationship__c oldAcr = Trigger.oldMap.get(acr.Id);
        List<Reporting_Requirement__c> rpts = [SELECT Contact__c FROM Reporting_Requirement__c WHERE Contact__c = :oldAcr.Contact__c];
        for(Reporting_Requirement__c rpt : rpts){
            rpt.Contact__c = acr.Contact__c;
            extRpt.add(rpt);
        }
    }
    update extRpt;
}