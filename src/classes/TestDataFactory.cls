public with sharing class TestDataFactory {
	public TestDataFactory() {
		
	}

	public static List<Account> createAccounts(Integer num){
		List<Account> lstAcc = new List<Account>();
		for(Integer i=0; i<num; i++){
			Account acc = new Account();
			acc.Name = 'Name'+i;
			lstAcc.add(acc);
		}
		return lstAcc;
	}
}