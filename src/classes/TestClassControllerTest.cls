@isTest
public class TestClassControllerTest {
	public static testMethod void getStatementMethodPositive(){
		TestClassController.getName = 'Testing 123';
		TestClassController.getLastName = 'Testing 123';
		TestClassController.getStatement('test1');
	}

	public static testMethod void getStatementMethodNegative(){
		TestClassController.getStatement('test2');
	}
}