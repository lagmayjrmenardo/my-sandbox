public with sharing class AccountController {

	public static String getName{get;set;}
	public static Integer getAge{get;set;}
	public static String getNewAccount{get;set;}
	public static Boolean isActive{get;set;}
	//public static HttpResponse getResponse{get;set;}
	public static List<Account> getAccounts{get;set;}


	public AccountController(){

	}
	
    public static List<Account> findAll() {
        return [SELECT id, name, Location__Latitude__s, Location__Longitude__s
                FROM Account
                WHERE Location__Latitude__s != NULL AND Location__Longitude__s != NULL
                LIMIT 50];
    }

	public static List<Account> getAllAccounts(){
		getAccounts = [SELECT Id, Name FROM Account];
		List<Account> newAccounts = new List<Account>();
		for(Account x: getAccounts){
			newAccounts.add(x);
		}

		return newAccounts;
	}

	public static String getName(String contactName){
		List<Contact> x = new List<Contact>();

		return 'samp;e';
	}


	public static HttpResponse getAccountCallout(){
		Http http = new Http();
		HttpRequest request = new HttpRequest();
		request.setEndpoint('https://th-apex-http-callout.herokuapp.com/animals');
		request.setMethod('POST');
		request.setHeader('Content-Type', 'application/json;charset=UTF-8');
		request.setBody('{"name": "mighty moose"}');
		HttpResponse response = http.send(request);
		return response;
	}

	    public static HttpResponse makeGetCallout() {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://th-apex-http-callout.herokuapp.com/animals');
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        // If the request is successful, parse the JSON response.
        if (response.getStatusCode() == 200) {
            // Deserializes the JSON string into collections of primitive data types.
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            // Cast the values in the 'animals' key as a list
            List<Object> animals = (List<Object>) results.get('animals');
            System.debug('Received the following animals:');
            for (Object animal: animals) {
                System.debug(animal);
            }
        }
        return response;
    }

    public static HttpResponse makePostCallout(String strName) {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://th-apex-http-callout.herokuapp.com/animals');
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json;charset=UTF-8');
        /* sample values: "mighty moose" */
        request.setBody('{"name": :strName}');
        HttpResponse response = http.send(request);
        // Parse the JSON response
        if (response.getStatusCode() != 201) {
            System.debug('The status code returned was not expected: ' +
                response.getStatusCode() + ' ' + response.getStatus());
        } else {
            System.debug(response.getBody());
        }
        return response;
    } 

    public static String test(){
        return 'Test';
    }
}