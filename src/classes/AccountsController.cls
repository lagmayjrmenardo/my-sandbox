public class AccountsController {
  public static Boolean isError{get;set;}
  public AccountsController(){
    isError = false;
  }
  @AuraEnabled
  public static List<Account> getAccounts() {
    return [SELECT Id, name, industry, Type, NumberOfEmployees, TickerSymbol, Phone
    FROM Account ORDER BY createdDate ASC];
  }

  @RemoteAction
  public static Boolean returnError(){
    return isError;
  }
}