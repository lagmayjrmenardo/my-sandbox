public class testOrg {
//-------------------------Trigger Handler--------------------
//public class afterAccountUpdateHandler {
   public static List<Opportunity> opportunityList;
   public static void handleBeforeUpdate( Set<Id> accountIdSet){
   
       Id profileId=userinfo.getProfileId();
       String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
       system.debug('ProfileName'+profileName);
   
   //List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
   //String MyProfileName = PROFILE[0].Name;
   
    if (profileName <> 'System Administrator') { //for sys admin checking
      opportunityList = [SELECT AccountId,Id,OwnerId FROM Opportunity where StageName IN('Business Won','Lost','Discard') and AccountId in : accountIdSet];
     }//for syst= admin checking
   }
   
   public static void handleAfterUpdate(){
 
       if(opportunityList.size()>0)
           update opportunityList;
   }
     
}