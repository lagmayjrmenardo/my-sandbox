global class BatchableReport implements Database.Batchable<sObject> {
	global Database.QueryLocator start(Database.BatchableContext BC)
	{
        String query = 'SELECT Id,Name,Phone FROM Account LIMIT 1';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        System.debug('working');
    }  
    global void finish(Database.BatchableContext BC)
    {
    }
}