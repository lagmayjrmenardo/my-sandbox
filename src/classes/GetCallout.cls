public class GetCallout {
	/**********************************************
	 * This is the EndPoint: 'https://docsample.herokuapp.com/xmlSample'
	 * This is the Method: 'GET'
	 *
	 **********************************************/

    public GetCallout(){
        String endPoint = 'https://docsample.herokuapp.com/xmlSample';
        String method = 'GET';
        System.debug(getResponse(endPoint, method).getBody());
        System.debug(getName('menardo').capitalize());
    }
    public static HttpResponse getResponse(String getEndPoint, String getMethod){
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(getEndPoint);
        req.setMethod(getMethod);
        req.setTimeout(30000);
        HttpResponse res = http.send(req);
        //System.debug(res.getBody());
        return res;
    }
    public static String getName(String strName){
        //strName.capitalize();
        return strName;
    }
}