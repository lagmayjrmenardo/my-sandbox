public class MyContactsController {
	public MyContactsController() {
		
	}

	@AuraEnabled
	public static List<Contact> getContacts(List<Id> accountIds){
		List<Contact> contactList = [SELECT Id, Name, AccountId, Phone, Email FROM Contact WHERE AccountId IN :accountIds];
		return contactList;
	}

	@AuraEnabled
	public static String getStringValue(String firstName){
		return 'Full Name: '+firstName;
	}
}