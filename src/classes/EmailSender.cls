global class EmailSender implements Messaging.InboundEmailHandler {
	global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
		Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
	    string replyTo = email.replyTo;
	    string fromName = email.fromName;
	    string fromAddress= email.fromAddress;
		String getBody = email.plainTextBody;
		String strEdit = getBody.replace('www.google.com', 'www.somethingelse.com');
		//System.debug(strEdit);

		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
		String[] toAddresses = new String[] {fromAddress}; 
		mail.setToAddresses(toAddresses);
		mail.setSubject(email.subject);  
		mail.setSaveAsActivity(false);  
		mail.setHtmlBody(strEdit);  
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
		return result;
	}
}