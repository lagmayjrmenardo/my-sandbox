public class ProductController {
	public static List<Product2> lstProducts{get;set;}
    public ProductController(){

    }

    public static List<Product2> getAllProducts(){
    	List<Product2> allProducts = [SELECT Id, Name, Description, ProductCode, Family FROM Product2];
    	return allProducts;
    }

}