public class TestClassController {
    public static String getName{get;set;}
    public static String getLastName{get;set;}
    
    public static String getStatement(String text){
        String returnText;
        if(text == 'test1'){
           returnText = 'Yes this is test 1';
        }else if(text == 'text2'){
            returnText = 'Yes this is test 2';
        }else{
            returnText = 'This is the default test';
        }
        
        return returnText;
    }
    
    public static List<Account> getAllAccount(){
        return [SELECT Id, Name FROM Account];
    }
    
    public static List<Account> getAccountByName(String accountName){
        return [SELECT Id, Name FROM Account WHERE Name = :accountName];
    }

    public static List<Account> getAccountById(String accountId){
        return [SELECT Id, Name FROM Account WHERE Id = :accountId];
    }
}