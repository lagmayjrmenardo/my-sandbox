public class AccountCtrl {
	public static String acrLink{get;set;}
	public static String oldContact{get;set;}
	public static List<APEX_Account_Contact_Relationship__c> x1{get;set;}
	public static List<APEX_Account_Contact_Relationship__c> getAllACR{get;set;}

	public AccountCtrl(){
		acrLink = getReference();
		oldContact = getACR().Contact__r.Name;
		x1 = getRelatedRelationship();

	}
	public static List<APEX_Account__c> getAccount(String accId){
		return [SELECT Id, Name FROM APEX_Account__c WHERE Id=:accId];
	}

	public static APEX_Account_Contact_Relationship__c getACR(){
		String accId = acrLink;
		APEX_Account_Contact_Relationship__c x = [SELECT Id, Contact__c, Contact__r.Name FROM APEX_Account_Contact_Relationship__c 
												  WHERE Account__c = :accId LIMIT 1];
												  return x;
	}

	public static List<APEX_Account_Contact_Relationship__c> getRelatedRelationship(){
		String accId = acrLink;
		List<APEX_Account_Contact_Relationship__c> x = [SELECT Contact__r.Name FROM APEX_Account_Contact_Relationship__c WHERE Account__c = :accId];
		return x;
	}

	public static String getReference(){
		PageReference pg = ApexPages.currentPage();
		String x = pg.getParameters().get('Id');
		return x;
	}

	public static List<Account> getStandardAccounts(){
		return [SELECT Id, Name FROM Account];
	}

	public static List<String> getAllNums(){
		return new String[]{'one','two','three'};
	}
}