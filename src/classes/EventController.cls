public class EventController {
	public static List<String> eventStartTime{get;set;}
	public static List<String> eventEndTime{get;set;}
	public static String x{get;set;}
	public static List<Event> getEvent{get;set;}
	public EventController() {
		getEvents();
	}

	public static List<Event> getEvents(){
		getEvent = new List<Event>();
		List<Event> getAllEvents = [SELECT StartDateTime, EndDateTime, Subject, What.Name , Who.Name, Who.Type, Location, Owner.Name
									FROM Event WHERE EventSubType='Event' AND Subject IN ('Call', 'Meeting') AND WhatId != NULL];
		for(Event singleEvent: getAllEvents){
			singleEvent.StartDateTime.time();
			singleEvent.EndDateTime.time();
			getEvent.add(singleEvent);
		}

		return getEvent;
	}

	public PageReference redirect(){
		PageReference x = new PageReference('https://lightningapp-sample-dev-ed.my.salesforce.com/00U/e?retURL=%2Fhome%2Fhome.jsp');
		return x;
	}
}