public class CartController {
	public CartController() {
		
	}
	
	@RemoteAction
	public static List<Product2> getAllProducts(){
		List<Product2> allProducts = [SELECT Id, Name FROM Product2];
		if(allProducts != null){
			return allProducts;
		}
		
		return null;
	}
}