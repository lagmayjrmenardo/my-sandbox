public class CityFinderCalloutController {

	public RestResponse RestResponse;

	public class RestResponse {
		public List<String> messages;
		public List<Result> result;
	}

	public class Result {
		public Integer id;
		public String country;
		public String name;
		public String abbr;
		public String area;
		public String largest_city;
		public String capital;
	}

	
	public static CityFinderCalloutController parse(String json) {
		return (CityFinderCalloutController) System.JSON.deserialize(json, CityFinderCalloutController.class);
	}
}