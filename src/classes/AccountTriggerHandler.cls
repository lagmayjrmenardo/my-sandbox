public class AccountTriggerHandler {
	public static Map<Id, Account> newMap{get;set;}
	public static Map<Id, Account> oldMap{get;set;}

	public AccountTriggerHandler(Map<Id, Account> triggerNew, Map<Id, Account> triggerOld){
		newMap = triggerNew;
		oldMap = triggerOld;
	}

	public void updateAccount(){

	}

}