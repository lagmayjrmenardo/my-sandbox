global with sharing class ContactJs {  
    /**
    * Webkul Software.
    *
    * @category  Webkul
    * @author    Webkul
    * @copyright Copyright (c) 2010-2016 Webkul Software Private Limited (https://webkul.com)
    * @license   https://store.webkul.com/license.html
    */
    public List<wrapContact> contactWrapper{get;set;}
    public static String getName{get;set;}
    public ContactJs() { 

    } // empty constructor    
    
    public List<wrapContact> getAllContact(String contactName){
        String x = '%'+contactName+'%';
        List<Contact> contactMember = [SELECT id, name, email FROM Contact WHERE name LIKE :x LIMIT 10];
        if(contactWrapper == null){
            contactWrapper = new List<wrapContact>();
            for(Contact con: contactMember){
                contactWrapper.add(new wrapContact(con));
            }
        }
        return contactWrapper;
    }
    @RemoteAction //the function to be called in remote action should use this annotation
    global static list<Contact> getcon() {
        //function should be static and global else it will throw error
        list<Contact> con1 = [SELECT id,name FROM contact limit 5];
        if(con1!=null && !con1.isEmpty()){        
            return con1;        
        }else{        
            return  new list<contact>();        
        }
    }
    
    @RemoteAction //selected contacts
    global static Contact selectedContacts(Id conId){
        Contact con = [SELECT id, name, email FROM Contact WHERE id=:conId];
        return con;
    }
    
    @RemoteAction
    global static List<Contact> getContact(String x){
        String a = '%'+x+'%';
        List<Contact> cons = [SELECT id, name, email FROM Contact WHERE name LIKE :a ORDER BY name ASC LIMIT 10 ];
        if(cons.size() > 0){
        	return cons;    
        }
            return null;
        
        
    }
    
    public class wrapContact{
        public Contact con{get;set;}
        public Boolean selectedContact{get;set;}
        public wrapContact(Contact contactItem){
            con = contactItem;
            selectedContact = false;
        }
    }
}