public with sharing class ContactController {

    @AuraEnabled
    public static List<Contact> findNearby(Double latitude, Double longitude, Double maxDistance) {
        return Database.query('SELECT Id, Name, MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry, Phone FROM contact' +
                       ' WHERE DISTANCE(Location__c, GEOLOCATION(' + latitude + ',' + longitude + '), \'mi\') < '+ maxDistance +
                       ' ORDER BY DISTANCE(Location__c, GEOLOCATION(' + latitude + ',' + longitude + '), \'mi\')');
        
    }
    
    @AuraEnabled
    public static List<Contact> findAll(){
        return [SELECT Id, Name, Phone FROM Contact LIMIT 50];
    }
    
    @AuraEnabled
    public static List<Contact> findByName(String contactName){
        String searchKey = '%'+contactName+'%';
        return [SELECT Id, Name, Phone FROM Contact WHERE Name=:searchKey LIMIT 50];
    }
    
    @AuraEnabled
    public static List<Contact> findById(String contactId){
        return [SELECT Id, Name, Title, Phone, MobilePhone, Account.Name FROM Contact WHERE Id=:contactId];
    }
    

}