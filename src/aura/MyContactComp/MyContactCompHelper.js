({
	fetchContacts : function(component, event, helper) {
		var action = component.get('c.getContacts');
		var accountId = component.get('v.recordId');
		action.setParams({
			accountIds: accountId
		});
		action.setCallback(this, function(response){
			var state = response.getState();
			console.log(state);
			if(state === 'SUCCESS'){
				var contactList = response.getReturnValue();
				console.log(...contactList);
				component.set("v.contactList", contactList);
			}else{
				alert('Error in getting data');
			}
		});
		$A.enqueueAction(action);
	},
	setContactList : function(component, event, helper){
		//var action = component.get('c.getStringValue');
		var fname = component.get('v.firstName');
		alert(fname);
	}
})